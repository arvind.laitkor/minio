Minio is an object storage server released under Apache License v2.0. It is compatible with Amazon S3 cloud storage service. It is best suited for storing unstructured data such as photos, videos, log files, backups and container / VM images. Size of an object can range from a few KBs to a maximum of 5TB.

Minio server is light enough to be bundled with the application stack, similar to NodeJS, Redis and MySQL.

Docker Container

Step 1-: Clone the docker-compose.yaml from  git@gitlab.com:arvind.laitkor/minio.git

	        $ git clone  git@gitlab.com:arvind.laitkor/minio.git

Step 2-: Run docker-compose 

	        $ cd minio
	 
	        $ docker-compose up -d

Step 3-: Get the infomation about the running docker container.
            
            $ docker ps
            
            Output-:
                CONTAINER ID        IMAGE                COMMAND                  CREATED             STATUS              PORTS                  NAMES
                
                18243989a165        minio/minio:latest   "minio server /export"   9 seconds ago       Up 8 seconds        0.0.0.0:80->9000/tcp   minio_web_1
                
Step 4-: Get the Access Key and Secret Key using below command.
            
            $ docker logs minio_web_1
            
            Output-:
            
            Endpoint:  http://172.18.0.2:9000  http://127.0.0.1:9000
            AccessKey: 94BDDG7K63HSYBFX2VYM
            SecretKey: IRqW/7NXRcl2xtHbsE2eDJRq9EXyjY+kjuy4e4Tv
            Region:    us-east-1
            SqsARNs:
            
            Browser Access:
               http://172.18.0.2:9000  http://127.0.0.1:9000
            
            Command-line Access: https://docs.minio.io/docs/minio-client-quickstart-guide
               $ mc config host add myminio http://172.18.0.2:9000 94BDDG7K63HSYBFX2VYM IRqW/7NXRcl2xtHbsE2eDJRq9EXyjY+kjuy4e4Tv
            
            Object API (Amazon S3 compatible):
               Go:         https://docs.minio.io/docs/golang-client-quickstart-guide
               Java:       https://docs.minio.io/docs/java-client-quickstart-guide
               Python:     https://docs.minio.io/docs/python-client-quickstart-guide
               JavaScript: https://docs.minio.io/docs/javascript-client-quickstart-guide

	
